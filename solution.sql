--Find all artists that has letter d in its name
SELECT * FROM artists WHERE name LIKE "%d%";

--find all songs that has length of < 230
SELECT * FROM songs WHERE length < 230;

--Join the albums and songs table 
SELECT album_title AS album_name,song_name,length as song_length FROM songs 
JOIN albums ON songs.album_id = albums.id;

--Join the artist and albums table 
SELECT * FROM artists 
JOIN albums ON artists.id = albums.artist_id and name LIKE "%A%";

--Sort the albums Z-A order 
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4 ;

--join the album and songs table 
SELECT * FROM albums 
JOIN songs ON songs.album_id = albums.id ORDER BY album_title DESC, song_name ASC;


SELECT * FROM artists JOIN albums
ON artists.id = albums.artist_id WHERE albums.album_title LIKE '%A%';
